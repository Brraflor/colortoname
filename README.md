This Java program takes in a image (e.g JPG) and internally splits the images n*n. It takes the RGB average of those blocks, if it falls between a a known
named color, then the output is the name of the color. The reason I wrote this program was to find out the name of a color used in a wallpaper. Works well with 
any digital created content, have not tested with real life images.
